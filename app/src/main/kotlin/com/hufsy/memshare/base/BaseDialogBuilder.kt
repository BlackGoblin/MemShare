/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.base

import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent

open class BaseDialogBuilder(context: Context, lifecycleOwner: LifecycleOwner) : AlertDialog.Builder(context), LifecycleObserver {

    private lateinit var dialog: AlertDialog

    init {
        @Suppress("LeakingThis")
        lifecycleOwner.lifecycle.addObserver(this)
    }

    override fun create(): AlertDialog {
        dialog = super.create()
        return dialog
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        dialog.cancel()
    }
}