/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.base

import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment()