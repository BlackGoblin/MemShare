/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.home

import androidx.lifecycle.ViewModel
import com.hufsy.memshare.business.persistance.NoteDatabaseHelper
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val noteDatabaseHelper: NoteDatabaseHelper) : ViewModel() {

    val notes = noteDatabaseHelper.fetchPaged()

    fun search(query: String) = noteDatabaseHelper.search(query)
}