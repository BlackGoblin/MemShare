/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.home

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager.VERTICAL
import com.hufsy.memshare.R
import com.hufsy.memshare.base.BaseFragment
import com.hufsy.memshare.common.model.Model
import com.hufsy.memshare.util.getInteger
import com.hufsy.memshare.util.gone
import com.hufsy.memshare.util.visible
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : BaseFragment(), View.OnClickListener, View.OnFocusChangeListener, SearchView.OnQueryTextListener, Observer<PagedList<Model.Note>> {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
    }

    private val listAdapter = HomeListAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        noteRecyclerView.layoutManager = StaggeredGridLayoutManager(context?.getInteger(R.integer.span) ?: 2, VERTICAL)
        noteRecyclerView.adapter = listAdapter

        fab.setOnClickListener(this)

        showNotes()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.home, menu)

        val searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.setOnQueryTextListener(this)
    }

    override fun onClick(view: View) {
        val directions = HomeFragmentDirections.navigateToNoteFragment(null)
        view.findNavController().navigate(directions)
    }

    override fun onFocusChange(view: View, focused: Boolean) {}

    override fun onQueryTextSubmit(query: String?) = false

    override fun onQueryTextChange(newText: String): Boolean {
        if (newText.isEmpty()) {
            viewModel.notes.removeObserver(this)
            showNotes()
        } else {
            viewModel.search(newText).observe(this, this)
        }
        return true
    }

    override fun onChanged(list: PagedList<Model.Note>?) {
        listAdapter.submitList(list)
        if (list == null || list.isEmpty()) {
            emptyTextView.visible()
        } else {
            emptyTextView.gone()
        }
    }

    private fun showNotes() {
        viewModel.notes.observe(this, this)
    }
}