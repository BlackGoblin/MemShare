/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.preview

import androidx.lifecycle.ViewModel
import com.hufsy.memshare.injection.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class PreviewBuilder {

    @ContributesAndroidInjector
    internal abstract fun previewFragment(): PreviewFragment

    @Binds
    @IntoMap
    @ViewModelKey(PreviewViewModel::class)
    internal abstract fun bindPreviewViewModel(viewModel: PreviewViewModel): ViewModel
}