/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.about

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class AboutBuilder {

    @ContributesAndroidInjector
    internal abstract fun aboutFragment(): AboutFragment
}