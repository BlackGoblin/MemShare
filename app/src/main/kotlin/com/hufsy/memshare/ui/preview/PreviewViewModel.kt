/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.preview

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.hufsy.memshare.business.file.IFileHelper
import com.hufsy.memshare.business.persistance.NoteDatabaseHelper
import com.hufsy.memshare.common.model.Model
import java.io.File
import javax.inject.Inject

class PreviewViewModel @Inject constructor(private val noteDatabaseHelper: NoteDatabaseHelper, private val fileHelper: IFileHelper) : ViewModel() {

    private var noteLiveData: LiveData<Model.Note?>? = null

    fun getNote(id: Long): LiveData<Model.Note?> {
        var temp = noteLiveData
        if (temp == null) {
            temp = noteDatabaseHelper.fetch(id)
            noteLiveData = temp
        }
        return temp
    }

    fun archiveNote(id: Long, archived: Boolean) = noteDatabaseHelper.archiveNote(id, archived)

    fun markNote(id: Long, marked: Boolean) = noteDatabaseHelper.markNote(id, marked)

    fun saveScreenshot(bitmap: Bitmap): File = fileHelper.save(bitmap)
}