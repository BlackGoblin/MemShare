/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.preview

import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.hufsy.memshare.R
import com.hufsy.memshare.base.BaseFragment
import com.hufsy.memshare.common.model.Model
import com.hufsy.memshare.ui.MainActivity
import com.hufsy.memshare.util.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_preview.*
import java.io.File
import javax.inject.Inject

class PreviewFragment : BaseFragment(), View.OnClickListener {

    private var note: Model.Note? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: PreviewViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PreviewViewModel::class.java)
    }

    private val id: Long by lazy {
        PreviewFragmentArgs.fromBundle(arguments).id.toLong()
    }

    private val title: String by lazy {
        PreviewFragmentArgs.fromBundle(arguments).title
    }

    private val imageName: String? by lazy {
        PreviewFragmentArgs.fromBundle(arguments).imageName
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).setSupportActionBar(previewToolbar)
        collapsingToolbarLayout.title = title
        imageView.showImage(imageName)

        fab.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.preview, menu)
        showNote(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.share -> {
            shareNote()
            true
        }
        R.id.archive -> {
            viewModel.archiveNote(id, true)
            activity?.showSnackbar(getString(R.string.messageArchived))
            true
        }
        R.id.unarchive -> {
            viewModel.archiveNote(id, false)
            activity?.showSnackbar(getString(R.string.messageUnarchived))
            true
        }
        R.id.mark -> {
            viewModel.markNote(id, true)
            activity?.showSnackbar(getString(R.string.messageMarked))
            true
        }
        R.id.unmark -> {
            viewModel.markNote(id, false)
            activity?.showSnackbar(getString(R.string.messageUnmarked))
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity).apply {
            toolbar.gone()
            previewToolbar.post {
                setSupportActionBar(previewToolbar)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        (activity as MainActivity).apply {
            toolbar.visible()
            initActionBar()
        }
    }

    override fun onClick(view: View) {
        val directions = PreviewFragmentDirections.navigateToNoteFragment(id.toString())
        view.findNavController().navigate(directions)
    }

    private fun showNote(menu: Menu) {
        viewModel.getNote(id).observe(this, Observer {
            it?.let { note ->
                this.note = note
                collapsingToolbarLayout.title = note.title
                descriptionTextView.text = note.description
                imageView.showImage(note.imageName)
                menu.findItem(R.id.archive).isVisible = !note.archived
                menu.findItem(R.id.unarchive).isVisible = note.archived
                menu.findItem(R.id.mark).isVisible = !note.marked
                menu.findItem(R.id.unmark).isVisible = note.marked
                menu.findItem(R.id.share).isVisible = true
            }
        })
    }

    private fun shareNote() {
        try {
            val bitmap = imageView.getScreenshot()
            val file: File = viewModel.saveScreenshot(bitmap)
            note?.share(imageView.context, file)
        } catch (ex: Exception) {
            note?.share(imageView.context, null)
        }
    }
}