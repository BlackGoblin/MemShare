/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.ui.home

import androidx.lifecycle.ViewModel
import com.hufsy.memshare.injection.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class HomeBuilder {

    @ContributesAndroidInjector
    internal abstract fun homeFragment(): HomeFragment

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel
}