/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.IntegerRes
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.hufsy.memshare.R
import com.hufsy.memshare.common.model.Model
import java.io.File

fun Context.getColorCompat(@ColorRes resource: Int): Int = ContextCompat.getColor(this, resource)

fun Context.getDimension(@DimenRes resource: Int): Int = resources.getDimensionPixelSize(resource)

fun Context.getInteger(@IntegerRes resource: Int) = resources.getInteger(resource)

fun Context.getDrawableCompat(@DrawableRes resource: Int) = ContextCompat.getDrawable(this, resource)

fun Context.isPermissionGranted(permission: String): Boolean = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

fun Activity.showSnackbar(message: String) = Snackbar.make(findViewById<View>(android.R.id.content), message, Snackbar.LENGTH_LONG).show()

fun View.visible() {
    visibility = VISIBLE
}

fun View.gone() {
    visibility = GONE
}

fun View.hideSoftKeyboard() {
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}

fun ImageView.showImage(string: String?) {
    try {
        showImage(Uri.parse(string))
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun ImageView.showImage(uri: Uri?) {
    Glide.with(context)
            .load(uri)
            .into(this)
}

fun ImageView.getScreenshot(): Bitmap = (drawable as BitmapDrawable).bitmap

fun Int.isPermissionGranted() = this == PackageManager.PERMISSION_GRANTED

fun Model.Note.share(context: Context, file: File?) {
    val shareIntent = Intent(Intent.ACTION_SEND)
    if (file != null) {
        val fileUri = FileProvider.getUriForFile(context, context.getString(R.string.AUTHORITY), file)
        shareIntent.type = "image/*"
        shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri)
    } else {
        shareIntent.type = "text/plain"
    }
    shareIntent.putExtra(Intent.EXTRA_SUBJECT, title)
    shareIntent.putExtra(Intent.EXTRA_TEXT, description)
    context.startActivity(Intent.createChooser(shareIntent, context.getString(R.string.labelShare)))
}