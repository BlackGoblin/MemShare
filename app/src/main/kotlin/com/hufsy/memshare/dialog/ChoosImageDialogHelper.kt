/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.dialog

import android.content.Context
import android.view.View
import androidx.lifecycle.LifecycleOwner
import com.hufsy.memshare.R
import com.hufsy.memshare.base.BaseDialogBuilder
import kotlinx.android.synthetic.main.dialog_choose_image.view.*

class ChoosImageDialogHelper(private val context: Context, lifecycleOwner: LifecycleOwner) {

    private val view: View by lazy {
        View.inflate(context, R.layout.dialog_choose_image, null)
    }

    private val dialog = BaseDialogBuilder(context, lifecycleOwner)
            .setView(view)
            .setTitle(R.string.chooseImage)
            .create()

    fun show(onChoiceMade: (Choice) -> Unit) {
        view.cameraTextView.setOnClickListener {
            onChoiceMade(Choice.CAMERA)
            dialog.dismiss()
        }

        view.galleryTextView.setOnClickListener {
            onChoiceMade(Choice.GALLERY)
            dialog.dismiss()
        }

        dialog.show()
    }

    enum class Choice {
        CAMERA, GALLERY
    }
}