/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.common.model

sealed class Model {

    data class Note(
            val id: Long?,
            val title: String,
            val description: String,
            val imageName: String?,
            val archived: Boolean = false,
            val marked: Boolean = false
    ) : Model()
}