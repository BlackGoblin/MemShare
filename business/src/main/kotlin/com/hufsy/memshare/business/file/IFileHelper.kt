/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.business.file

import android.graphics.Bitmap
import java.io.File

interface IFileHelper {

    /**
     * saves a bitmap into a file
     */
    fun save(bitmap: Bitmap): File

    /**
     * creates an image file
     */
    fun createImageFile(): File
}