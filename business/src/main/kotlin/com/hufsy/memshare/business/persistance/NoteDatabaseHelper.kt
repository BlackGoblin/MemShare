/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.business.persistance

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.hufsy.memshare.business.persistance.dao.NoteDao
import com.hufsy.memshare.business.persistance.domain.Table
import com.hufsy.memshare.common.model.Model
import javax.inject.Inject

class NoteDatabaseHelper @Inject constructor(private val dao: NoteDao) : BaseDatabaseHelper<Table.Note, Model.Note>(dao) {

    override fun map(table: Table.Note): Model.Note = table.map()

    override fun unmap(model: Model.Note): Table.Note = model.map()

    fun fetchPaged(archived: Boolean = false, pageSize: Int = 10): LiveData<PagedList<Model.Note>> {
        val factory = dao.selectPaged(archived).mapByPage { list -> list.map { it.map() } }
        return LivePagedListBuilder(factory, pageSize).build()
    }

    fun fetchMarkedPaged(pageSize: Int = 10): LiveData<PagedList<Model.Note>> {
        val factory = dao.selectMarkedPaged().mapByPage { list -> list.map { it.map() } }
        return LivePagedListBuilder(factory, pageSize).build()
    }

    fun archiveNote(id: Long, archived: Boolean) = dao.archiveNote(id, archived)

    fun markNote(id: Long, marked: Boolean) = dao.markNote(id, marked)

    fun search(query: String, pageSize: Int = 10): LiveData<PagedList<Model.Note>> {
        val searchTerm = "%$query%"
        val factory = dao.search(searchTerm).mapByPage { list -> list.map { it.map() } }
        return LivePagedListBuilder(factory, pageSize).build()
    }
}