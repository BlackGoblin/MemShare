/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.business.persistance

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.hufsy.memshare.business.persistance.dao.BaseDao
import com.hufsy.memshare.business.persistance.domain.Table
import com.hufsy.memshare.common.model.Model

/**
 * it is responsible of hide the persistence complexities
 */
abstract class BaseDatabaseHelper<T : Table, M : Model>(private val dao: BaseDao<T>) {

    /**
     * fetches an entity and transform it into a model
     *
     * @param id the id of the entity to be fetched
     *
     * @return the transformed entity into a model
     */
    open fun fetch(id: Long): LiveData<M?> = Transformations.map(dao.select(id)) {
        if (it != null) {
            map(it)
        } else {
            null
        }
    }

    /**
     * fetches all the entities and transform all of them into a list of model
     *
     * @return a list of transformed entities into a list of models
     */
    open fun fetch(): LiveData<List<M>> = Transformations.map(dao.select()) { it.map { map(it) } }

    /**
     * transform a model into its corresponding entity and persist it
     *
     * @param model the model to be persisted
     */
    open fun persist(model: M) {
        if (dao.insert(unmap(model)) < 0) {
            dao.update(unmap(model))
        }
    }

    /**
     * persists a list of models that are first mapped into their corresponding entities
     *
     * @param models the models to be persisted
     */
    open fun persist(models: List<M>) = dao.insert(models.map { unmap(it) })

    /**
     * transform a model into its corresponding entity and remove it
     *
     * @param model the model to be removed
     */
    fun remove(model: M) = dao.delete(unmap(model))

    /**
     * removes a list of models that are first mapped into their corresponding entities
     *
     * @param models the models to be removed
     */
    fun remove(models: List<M>) = dao.delete(models.map { unmap(it) })

    /**
     * clears all the corresponding entities with the T model
     */
    fun clear() = dao.truncate()

    /**
     * maps a entity into its corresponding model
     */
    protected abstract fun map(table: T): M

    /**
     * maps a model into its corresponding entity
     */
    protected abstract fun unmap(model: M): T
}