/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.business.persistance.dao

import androidx.lifecycle.LiveData
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Update
import com.hufsy.memshare.business.persistance.domain.Table

/**
 * every other dao should extend base dao
 */
interface BaseDao<T : Table> {

    fun select(id: Long): LiveData<T?>

    fun select(): LiveData<List<T>>

    @Insert(onConflict = IGNORE)
    fun insert(t: T): Long

    @Insert(onConflict = REPLACE)
    fun insert(ts: List<T>)

    @Update(onConflict = REPLACE)
    fun update(t: T)

    @Update(onConflict = REPLACE)
    fun update(ts: List<T>)

    @Delete
    fun delete(t: T)

    @Delete
    fun delete(ts: List<T>)

    fun truncate()
}