/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.business.persistance.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.hufsy.memshare.business.persistance.domain.Table

@Dao
interface NoteDao : BaseDao<Table.Note> {

    @Query("SELECT * FROM Notes WHERE id = :id")
    override fun select(id: Long): LiveData<Table.Note?>

    @Query("SELECT * FROM Notes")
    override fun select(): LiveData<List<Table.Note>>

    @Query("DELETE FROM Notes")
    override fun truncate()

    @Query("SELECT * FROM Notes WHERE archived = :archived")
    fun selectPaged(archived: Boolean): DataSource.Factory<Int, Table.Note>

    @Query("SELECT * FROM Notes WHERE marked = :marked")
    fun selectMarkedPaged(marked: Boolean = true): DataSource.Factory<Int, Table.Note>

    @Query("SELECT * FROM Notes WHERE marked = :marked")
    fun selectFavoritesPaged(marked: Boolean = true): DataSource.Factory<Int, Table.Note>

    @Query("UPDATE Notes SET archived = :archived WHERE id = :id")
    fun archiveNote(id: Long, archived: Boolean)

    @Query("UPDATE Notes SET marked = :marked WHERE id = :id")
    fun markNote(id: Long, marked: Boolean)

    @Query("SELECT * FROM Notes WHERE title LIKE :query OR description LIKE :query")
    fun search(query: String): DataSource.Factory<Int, Table.Note>
}