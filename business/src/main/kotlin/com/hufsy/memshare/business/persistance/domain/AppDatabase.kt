/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.business.persistance.domain

import androidx.room.Database
import androidx.room.RoomDatabase
import com.hufsy.memshare.business.persistance.dao.NoteDao

@Database(entities = [Table.Note::class],
        version = 1,
        exportSchema = true)
abstract class AppDatabase : RoomDatabase() {

    abstract fun noteDao(): NoteDao
}