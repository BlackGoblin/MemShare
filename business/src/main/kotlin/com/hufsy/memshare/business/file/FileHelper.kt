/*
 * Copyright (c) 2018.
 */

package com.hufsy.memshare.business.file

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class FileHelper @Inject constructor(private val context: Context) : IFileHelper {

    override fun save(bitmap: Bitmap): File {
        val path = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val file = File(path, System.currentTimeMillis().toString() + ".png")
        val fileOutputStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
        file.createNewFile()
        fileOutputStream.close()
        return file
    }

    override fun createImageFile(): File {
        val path = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val imageFileName = System.currentTimeMillis().toString()
        return File.createTempFile(imageFileName, ".jpg", path)
    }
}